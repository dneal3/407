﻿using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public CharacterController2D e_controller;
    public Animator animator;

    public Transform target;

    //int playerDmg;
    //int health;
    bool patrolling = false;
    bool attacking = false;
    float move_num = 0f;
    public float runSpeed = 40f;

    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("Speed", Mathf.Abs(0f));
        if (!patrolling)
        {
            EnemyMovement();
        }
    }

    private void EnemyMovement()
    {
        if (((target.position.x - this.transform.position.x) < 1) && ((target.position.x - this.transform.position.x) > -1))
        {
            AttkPlayer();
            return;
        }
        if (((target.position.x - this.transform.position.x) >= -10) && ((target.position.x - this.transform.position.x) < 0))
        {
            animator.SetBool("Attacking", false);
            MoveLeft();
            return;
        }
        else if (((target.position.x - this.transform.position.x) <= 10) && ((target.position.x - this.transform.position.x) > 0))
        {
            animator.SetBool("Attacking", false);
            MoveRight();
            return;
        }
        else
        {
            animator.SetBool("Attacking", false);
            Patrolling();
            return;
        }
    }

    private void MoveRight()
    {
        // Move right toward the player when player is within 10 Units
        animator.SetFloat("Speed", 15f);
        e_controller.Move(15f * Time.deltaTime, false, false);
    }

    private void MoveLeft()
    {
        // Move left toward the player when player is within 10 Units
        animator.SetFloat("Speed", 15f);
        e_controller.Move(-15f * Time.deltaTime, false, false);
    }

    private void Patrolling()
    {
        // this is what the enemy does when the player is not within 10 Units
        move_num = Random.Range(-5f, 5f);
        move_num = move_num * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(move_num));
        e_controller.Move(move_num * Time.deltaTime, false, false);
        StartCoroutine(WaitMove());

    }
    private void AttkPlayer()
    {
        animator.SetBool("Attacking", true);
        //damaging the player would happen here prolly.
    }

    IEnumerator WaitMove()
    {
        patrolling = true;
        yield return new WaitForSeconds(3f);
        patrolling = false;
    }
}
