using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public CharacterController2D controller;
    public Animator animator;
    public float runSpeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    bool block = false;
    bool exiting = false;

    //Method is called once per frame
    void Update () {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
        if (Input.GetButtonDown("Fire3"))
        {
            block = true;
            animator.SetBool("Blocking", true);
            
        } else if(Input.GetButtonUp("Fire3"))
        {
            block = false;
            animator.SetBool("Blocking", false);
            
        } else if(Input.GetButtonDown("Fire1"))
        {
            animator.SetBool("Attack", true);
        } else if(Input.GetButtonUp("Fire1"))
        {
            animator.SetBool("Attack", false);
        }
        
        
    }

    void FixedUpdate () {
        //Moving the character
        if(exiting)
        {
            horizontalMove = 20;
            animator.SetBool("Exit", true);
        }
        else if(block)
        {
            horizontalMove *= 0.3f;
            
        }
        
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        exiting = true;
    }
}
  
    
